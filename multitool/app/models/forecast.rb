class Forecast

  attr_accessor :date, :day, :high, :low, :text

  def self.from_hash(hash)
    Forecast.new.tap do |f|
      f.date  = hash["date"]
      f.day   = hash["day"]
      f.high  = hash["high"]
      f.low   = hash["low"]
      f.text  = hash["text"]
    end
  end

end
