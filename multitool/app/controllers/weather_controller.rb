class WeatherController < ActionController::Base

  def index
    if request.post? and params[:zip]
      redirect_to weather_for_zip_url zip: params[:zip]
    else
      render "weather/index"
    end
  end

  # START: location
  def location
    # uncomment this to simulate slower responses
    # sleep 5.seconds
    @report = WeatherReport.for_location(params["name"])
    render "weather/zip"
  end
  # END: location

  def zip
    @report = WeatherReport.for_zip(params[:zip])
    render "weather/zip"
  end

end
