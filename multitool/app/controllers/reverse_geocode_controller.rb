class ReverseGeocodeController < ActionController::Base

  def reverse_geocode
    @geocode_response = ReverseGeocodeRequest.for(params[:lat], params[:long])
    render "reverse_geocode/reverse_geocode"
  end

end
