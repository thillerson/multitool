module PermissiveCorsBehavior
  extend ActiveSupport::Concern

  included do
    before_filter :add_permissive_cors_headers
  end

  def add_permissive_cors_headers
    headers['Access-Control-Allow-Origin'] = '*'
  end

end
