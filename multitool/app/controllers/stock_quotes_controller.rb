require 'net/http'

class StockQuotesController < ActionController::Base

  def index
    @quote_list = StockQuote.for(params[:symbols])
    render "stock_quotes/stock_quotes"
  end

end
