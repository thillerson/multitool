class CreateNotes < ActiveRecord::Migration
  def change
    create_table :notes do |t|
      t.string :title
      t.string :body
      t.string :latitude
      t.string :longitude
      t.string :created_where

      t.timestamps
    end
  end
end
